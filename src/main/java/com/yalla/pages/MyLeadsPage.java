package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class MyLeadsPage extends Annotations {

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how= How.LINK_TEXT, using ="Create Lead") WebElement eleCreateLead;
	@And("click test lead")
	public CreateLeadPage clickCreateLeads()
	{
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	@FindBy(how= How.LINK_TEXT, using ="Find Leads") WebElement eleFindLead;
	public FindLeadsPage clickFindLeads()
	{
		click(eleFindLead);
		return new FindLeadsPage();
	}
}
