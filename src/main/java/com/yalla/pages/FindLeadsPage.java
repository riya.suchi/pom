package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.yalla.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations {

	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "(//div/div/input[@name='firstName'])[3]")
	WebElement eleFirstName;

	public FindLeadsPage enterFirstName(String firstName) {
		clearAndType(eleFirstName, firstName);
		return this;
	}

	@FindBy(how = How.XPATH, using = "(//div/div/input[@name='companyName'])[2]")
	WebElement eleCompanyName;

	public FindLeadsPage enterCompanyName(String companyName) {
		clearAndType(eleCompanyName, companyName);
		return this;
	}

	@FindBy(how = How.XPATH, using = "//button[text()='Find Leads']")
	WebElement eleFindLeads;

	public FindLeadsPage clickFindLeads() {
		click(eleFindLeads);
		return this;
	}

	@FindBy(how = How.XPATH, using = "(//tr/td/div/a[@class='linktext'])[1]")
	WebElement eleLeadId;

	public ViewLeadsPage clickLeadId() {
		click(eleLeadId);
		return new ViewLeadsPage();
	}
}
