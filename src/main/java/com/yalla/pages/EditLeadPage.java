package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class EditLeadPage extends Annotations {

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "updateLeadForm_firstName")
	WebElement eleFirstName;

	public EditLeadPage editFirstName(String data) {
		clearAndType(eleFirstName, data);
		return this;
	}

	@FindBy(how = How.NAME, using = "submitButton")
	WebElement eleUpdate;

	public ViewLeadsPage clickUpdate() {
		click(eleUpdate);
		return new ViewLeadsPage();
	}
}
