package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class ViewLeadsPage extends Annotations {

	public ViewLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using = "Logout")
	WebElement eleLogout;
	@And("verify it and close")
	public void clickLogout() {
		click(eleLogout);
	}

	/*@FindBy(how = How.LINK_TEXT, using = "Delete")
	WebElement eleDelete;

	public void clickDelete() {
		click(eleDelete);

	}*/

	@FindBy(how = How.LINK_TEXT, using = "Edit")
	WebElement eleEdit;

	public EditLeadPage clickEdit() {
		click(eleEdit);
		return new EditLeadPage();

	}
}
