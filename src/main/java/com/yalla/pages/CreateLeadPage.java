package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class CreateLeadPage extends Annotations {

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "createLeadForm_companyName")
	
	WebElement eleCompName;
	@And("enter companyname as (.*)")
	public CreateLeadPage enterCompName(String data) {
		clearAndType(eleCompName, data);
		return this;
	}

	@FindBy(how = How.ID, using = "createLeadForm_firstName")
	WebElement eleFirstName;
	@And("enter firstname as (.*)")
	public CreateLeadPage enterFirstName(String data) {
		clearAndType(eleFirstName, data);
		return this;
	}

	@FindBy(how = How.ID, using = "createLeadForm_lastName")
	WebElement eleLastName;
	@And("enter lastname as (.*)")
	public CreateLeadPage enterLastName(String data) {
		clearAndType(eleLastName, data);
		return this;
	}

	@FindBy(how = How.NAME, using = "submitButton")
	WebElement eleSubmit;
	@And("click submit")
	public ViewLeadsPage clickCreateButton() {
		click(eleSubmit);
		return new ViewLeadsPage();
	}

}
