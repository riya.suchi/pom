package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLeads extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC002_EditLeads";
		testcaseDec = "Login into leaftaps and edit leads";
		author = "Suriya";
		category = "smoke";
		excelFileName = "TC003_EditLeads";
	}

	@Test(dataProvider = "fetchData")
	public void createLead(String uName, String pwd, String fname, String cname, String eFname) {
		new LoginPage().enterUserName(uName).enterPassWord(pwd).clickLogin().clickCRMFSA().clickLeads().clickFindLeads()
				.enterFirstName(fname).enterCompanyName(cname).clickFindLeads().clickLeadId().clickEdit()
				.editFirstName(eFname).clickUpdate().clickLogout();
	}

}
