package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLeads";
		testcaseDec = "Login into leaftaps and create leads";
		author = "Suriya";
		category = "smoke";
		excelFileName = "TC001";
	}

	@Test(dataProvider = "fetchData")
	public void createLead(String uName, String pwd, String cname, String fname, String lname) {
		new LoginPage().enterUserName(uName).enterPassWord(pwd).clickLogin().clickCRMFSA().clickLeads()
				.clickCreateLeads().enterCompName(cname).enterFirstName(fname).enterLastName(lname).clickCreateButton()
				.clickLogout();

	}

}
