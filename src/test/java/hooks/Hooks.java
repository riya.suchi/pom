package hooks;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.yalla.selenium.api.base.SeleniumBase;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends SeleniumBase {
	//public static ExtentHtmlReporter reporter;
	//public static ExtentReports extent;
	//public static ExtentTest test;

	public static String testcaseName, testcaseDec, author, category;

	@Before
	public void beforeScenario(Scenario s) {
		/*WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);*/
		reporter = new ExtentHtmlReporter("./reports/result.html");
		reporter.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(reporter);
		test = extent.createTest(s.getName(), s.getId());
		test.assignAuthor("Suriya");
		test.assignCategory("Smoke");
		startApp("chrome", "http://leaftaps.com/opentaps");

	}

	@After
	public void AfterScenario(Scenario s) {
		close();
	}
}
