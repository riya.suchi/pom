/*package steps;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	ChromeDriver driver;
	@Given("open the browser")
	public void openTheBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Given("maximize the browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();	
	}

	@Given("Load the url as (.*)")
	public void loadTheUrlAsHttpLeaftapsComOpentaps(String url) {
		driver.get(url);
		System.out.println(url);
	}

	@Given("enter the username as (.*)")
	public void enterTheUsernameAsDemoSalesManager(String uname) {
		driver.findElementById("username").sendKeys(uname);
	}

	@Given("enter the password as (.*)")
	public void enterThePasswordAsCrmsfa(String password) {
		driver.findElementById("password").sendKeys(password);
	}

	@Given("click login")
	public void clickLogin() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Given("click crmfsa link")
	public void clickCrmfsaLink() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("click lead")
	public void clickLead() {
		driver.findElementByLinkText("Leads").click();
	}

	@Given("click test lead")
	public void clickTestLead() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@Given("enter firstname as (.*)")
	public void enterFirstnameAsAishu(String fname) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);		
	}

	@Given("enter lastname as (.*)")
	public void enterLastnameAsP(String lname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	}

	@Given("enter companyname as (.*)")
	public void enterCompanynameAsTCS(String cname) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
	}

	@When("click submit")
	public void clickSubmit() {
		driver.findElementByName("submitButton").click();
	}

	@Then("verify it and close")
	public void verifyIt() {
		System.out.println("Created successfully");
		driver.close();
	}

}
*/