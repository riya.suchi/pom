package runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src\\test\\java\\features\\CreateLead_ScenarioOutline.feature", glue = { "com.yalla.pages",
		"hooks" }, monochrome = true)
// tag name can be anything and it supports "and", "or" and not(~))
public class Runner {

}
